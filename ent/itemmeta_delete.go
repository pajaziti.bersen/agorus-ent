// Code generated by ent, DO NOT EDIT.

package ent

import (
	"context"
	"fmt"

	"entgo.io/ent/dialect/sql"
	"entgo.io/ent/dialect/sql/sqlgraph"
	"entgo.io/ent/schema/field"
	"gitlab.com/pajaziti.bersen/agorus-ent/ent/itemmeta"
	"gitlab.com/pajaziti.bersen/agorus-ent/ent/predicate"
)

// ItemMetaDelete is the builder for deleting a ItemMeta entity.
type ItemMetaDelete struct {
	config
	hooks    []Hook
	mutation *ItemMetaMutation
}

// Where appends a list predicates to the ItemMetaDelete builder.
func (imd *ItemMetaDelete) Where(ps ...predicate.ItemMeta) *ItemMetaDelete {
	imd.mutation.Where(ps...)
	return imd
}

// Exec executes the deletion query and returns how many vertices were deleted.
func (imd *ItemMetaDelete) Exec(ctx context.Context) (int, error) {
	var (
		err      error
		affected int
	)
	if len(imd.hooks) == 0 {
		affected, err = imd.sqlExec(ctx)
	} else {
		var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
			mutation, ok := m.(*ItemMetaMutation)
			if !ok {
				return nil, fmt.Errorf("unexpected mutation type %T", m)
			}
			imd.mutation = mutation
			affected, err = imd.sqlExec(ctx)
			mutation.done = true
			return affected, err
		})
		for i := len(imd.hooks) - 1; i >= 0; i-- {
			if imd.hooks[i] == nil {
				return 0, fmt.Errorf("ent: uninitialized hook (forgotten import ent/runtime?)")
			}
			mut = imd.hooks[i](mut)
		}
		if _, err := mut.Mutate(ctx, imd.mutation); err != nil {
			return 0, err
		}
	}
	return affected, err
}

// ExecX is like Exec, but panics if an error occurs.
func (imd *ItemMetaDelete) ExecX(ctx context.Context) int {
	n, err := imd.Exec(ctx)
	if err != nil {
		panic(err)
	}
	return n
}

func (imd *ItemMetaDelete) sqlExec(ctx context.Context) (int, error) {
	_spec := &sqlgraph.DeleteSpec{
		Node: &sqlgraph.NodeSpec{
			Table: itemmeta.Table,
			ID: &sqlgraph.FieldSpec{
				Type:   field.TypeUUID,
				Column: itemmeta.FieldID,
			},
		},
	}
	if ps := imd.mutation.predicates; len(ps) > 0 {
		_spec.Predicate = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	affected, err := sqlgraph.DeleteNodes(ctx, imd.driver, _spec)
	if err != nil && sqlgraph.IsConstraintError(err) {
		err = &ConstraintError{msg: err.Error(), wrap: err}
	}
	return affected, err
}

// ItemMetaDeleteOne is the builder for deleting a single ItemMeta entity.
type ItemMetaDeleteOne struct {
	imd *ItemMetaDelete
}

// Exec executes the deletion query.
func (imdo *ItemMetaDeleteOne) Exec(ctx context.Context) error {
	n, err := imdo.imd.Exec(ctx)
	switch {
	case err != nil:
		return err
	case n == 0:
		return &NotFoundError{itemmeta.Label}
	default:
		return nil
	}
}

// ExecX is like Exec, but panics if an error occurs.
func (imdo *ItemMetaDeleteOne) ExecX(ctx context.Context) {
	imdo.imd.ExecX(ctx)
}
