// Code generated by ent, DO NOT EDIT.

package ent

import (
	"time"

	"github.com/google/uuid"
	"gitlab.com/pajaziti.bersen/agorus-ent/ent/item"
	"gitlab.com/pajaziti.bersen/agorus-ent/ent/itemmeta"
	"gitlab.com/pajaziti.bersen/agorus-ent/ent/location"
	"gitlab.com/pajaziti.bersen/agorus-ent/ent/schema"
	"gitlab.com/pajaziti.bersen/agorus-ent/ent/user"
)

// The init function reads all schema descriptors with runtime code
// (default values, validators, hooks and policies) and stitches it
// to their package variables.
func init() {
	itemFields := schema.Item{}.Fields()
	_ = itemFields
	// itemDescRefID is the schema descriptor for ref_id field.
	itemDescRefID := itemFields[5].Descriptor()
	// item.DefaultRefID holds the default value on creation for the ref_id field.
	item.DefaultRefID = itemDescRefID.Default.(func() string)
	// itemDescCurrency is the schema descriptor for currency field.
	itemDescCurrency := itemFields[9].Descriptor()
	// item.DefaultCurrency holds the default value on creation for the currency field.
	item.DefaultCurrency = itemDescCurrency.Default.(string)
	// itemDescCondition is the schema descriptor for condition field.
	itemDescCondition := itemFields[11].Descriptor()
	// item.DefaultCondition holds the default value on creation for the condition field.
	item.DefaultCondition = itemDescCondition.Default.(string)
	// itemDescStock is the schema descriptor for stock field.
	itemDescStock := itemFields[12].Descriptor()
	// item.DefaultStock holds the default value on creation for the stock field.
	item.DefaultStock = itemDescStock.Default.(int)
	// itemDescCreatedAt is the schema descriptor for created_at field.
	itemDescCreatedAt := itemFields[13].Descriptor()
	// item.DefaultCreatedAt holds the default value on creation for the created_at field.
	item.DefaultCreatedAt = itemDescCreatedAt.Default.(func() time.Time)
	// itemDescUpdatedAt is the schema descriptor for updated_at field.
	itemDescUpdatedAt := itemFields[14].Descriptor()
	// item.DefaultUpdatedAt holds the default value on creation for the updated_at field.
	item.DefaultUpdatedAt = itemDescUpdatedAt.Default.(func() time.Time)
	// item.UpdateDefaultUpdatedAt holds the default value on update for the updated_at field.
	item.UpdateDefaultUpdatedAt = itemDescUpdatedAt.UpdateDefault.(func() time.Time)
	// itemDescID is the schema descriptor for id field.
	itemDescID := itemFields[0].Descriptor()
	// item.DefaultID holds the default value on creation for the id field.
	item.DefaultID = itemDescID.Default.(func() uuid.UUID)
	itemmetaFields := schema.ItemMeta{}.Fields()
	_ = itemmetaFields
	// itemmetaDescCreatedAt is the schema descriptor for created_at field.
	itemmetaDescCreatedAt := itemmetaFields[3].Descriptor()
	// itemmeta.DefaultCreatedAt holds the default value on creation for the created_at field.
	itemmeta.DefaultCreatedAt = itemmetaDescCreatedAt.Default.(func() time.Time)
	// itemmetaDescUpdatedAt is the schema descriptor for updated_at field.
	itemmetaDescUpdatedAt := itemmetaFields[4].Descriptor()
	// itemmeta.DefaultUpdatedAt holds the default value on creation for the updated_at field.
	itemmeta.DefaultUpdatedAt = itemmetaDescUpdatedAt.Default.(func() time.Time)
	// itemmeta.UpdateDefaultUpdatedAt holds the default value on update for the updated_at field.
	itemmeta.UpdateDefaultUpdatedAt = itemmetaDescUpdatedAt.UpdateDefault.(func() time.Time)
	// itemmetaDescID is the schema descriptor for id field.
	itemmetaDescID := itemmetaFields[0].Descriptor()
	// itemmeta.DefaultID holds the default value on creation for the id field.
	itemmeta.DefaultID = itemmetaDescID.Default.(func() uuid.UUID)
	locationFields := schema.Location{}.Fields()
	_ = locationFields
	// locationDescCreatedAt is the schema descriptor for created_at field.
	locationDescCreatedAt := locationFields[3].Descriptor()
	// location.DefaultCreatedAt holds the default value on creation for the created_at field.
	location.DefaultCreatedAt = locationDescCreatedAt.Default.(func() time.Time)
	// locationDescUpdatedAt is the schema descriptor for updated_at field.
	locationDescUpdatedAt := locationFields[4].Descriptor()
	// location.DefaultUpdatedAt holds the default value on creation for the updated_at field.
	location.DefaultUpdatedAt = locationDescUpdatedAt.Default.(func() time.Time)
	// location.UpdateDefaultUpdatedAt holds the default value on update for the updated_at field.
	location.UpdateDefaultUpdatedAt = locationDescUpdatedAt.UpdateDefault.(func() time.Time)
	// locationDescID is the schema descriptor for id field.
	locationDescID := locationFields[0].Descriptor()
	// location.DefaultID holds the default value on creation for the id field.
	location.DefaultID = locationDescID.Default.(func() uuid.UUID)
	userFields := schema.User{}.Fields()
	_ = userFields
	// userDescActive is the schema descriptor for active field.
	userDescActive := userFields[6].Descriptor()
	// user.DefaultActive holds the default value on creation for the active field.
	user.DefaultActive = userDescActive.Default.(bool)
	// userDescCreatedAt is the schema descriptor for created_at field.
	userDescCreatedAt := userFields[7].Descriptor()
	// user.DefaultCreatedAt holds the default value on creation for the created_at field.
	user.DefaultCreatedAt = userDescCreatedAt.Default.(func() time.Time)
	// userDescUpdatedAt is the schema descriptor for updated_at field.
	userDescUpdatedAt := userFields[8].Descriptor()
	// user.DefaultUpdatedAt holds the default value on creation for the updated_at field.
	user.DefaultUpdatedAt = userDescUpdatedAt.Default.(func() time.Time)
	// user.UpdateDefaultUpdatedAt holds the default value on update for the updated_at field.
	user.UpdateDefaultUpdatedAt = userDescUpdatedAt.UpdateDefault.(func() time.Time)
	// userDescRole is the schema descriptor for role field.
	userDescRole := userFields[9].Descriptor()
	// user.DefaultRole holds the default value on creation for the role field.
	user.DefaultRole = userDescRole.Default.(string)
	// userDescID is the schema descriptor for id field.
	userDescID := userFields[0].Descriptor()
	// user.DefaultID holds the default value on creation for the id field.
	user.DefaultID = userDescID.Default.(func() uuid.UUID)
}
