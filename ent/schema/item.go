package schema

import (
	"time"

	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
	"github.com/google/uuid"
)

// Item holds the schema definition for the Item entity.
type Item struct {
	ent.Schema
}

// Fields of the Item.
func (Item) Fields() []ent.Field {
	return []ent.Field{
		field.UUID("id", uuid.UUID{}).Default(uuid.New),
		field.String("name"),
		field.Float("amount"),
		field.Float("original_amount"),
		field.UUID("user_id", uuid.UUID{}),
		field.String("ref_id").Optional().Unique().DefaultFunc(func() string {
			return uuid.New().String()
		}),
		field.String("buy_url").Optional(),
		field.String("thumbnail").Optional(),
		field.String("sku").Optional(),
		field.String("currency").Default("EUR"),
		field.Float("native_amount").Optional(),
		field.String("condition").Default("NOT_SPECIFIED"),
		field.Int("stock").Default(1),
		field.Time("created_at").Default(time.Now),
		field.Time("updated_at").Default(time.Now).UpdateDefault(time.Now),
	}
}

// Edges of the Item.
func (Item) Edges() []ent.Edge {
	return []ent.Edge{
		edge.To("user", User.Type).Unique().Field("user_id").Required(),
		edge.To("meta", ItemMeta.Type),
	}
}
