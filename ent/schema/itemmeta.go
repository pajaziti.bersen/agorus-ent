package schema

import (
	"time"

	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
	"github.com/google/uuid"
)

// ItemMeta holds the schema definition for the ItemMeta entity.
type ItemMeta struct {
	ent.Schema
}

// Fields of the ItemMeta.
func (ItemMeta) Fields() []ent.Field {
	return []ent.Field{
		field.UUID("id", uuid.UUID{}).Default(uuid.New),
		field.String("key"),
		field.String("value"),
		field.Time("created_at").Default(time.Now),
		field.Time("updated_at").Default(time.Now).UpdateDefault(time.Now),
		field.UUID("item_id", uuid.UUID{}),
		field.UUID("metafilter_id", uuid.UUID{}),
	}
}

// Edges of the ItemMeta.
func (ItemMeta) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("item", Item.Type).
			Ref("meta").
			Unique().
			Required().
			Field("item_id"),
	}
}
